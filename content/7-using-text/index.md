# Using Text

![](images/1.png)

#### How do I use text effectively to support a multimedia story?

---

In this lesson you will learn:

* The different roles text can play in a story
* How to identify good information for the text elements of your story
* How to write a short text post
* How to write keywords to accompany video, audio, photo, or other media elements

---

The Internet in general and mobile devices in particular are visual environments. Pictures, video, and audio are powerful storytelling tools on their own or together. No matter how these media elements are used, each requires some text.

The written word plays several essential roles online. It can come in the form of a short, descriptive news “explainer,” a photo/video caption, or titles and keywords/tags for other media content. If you have an Internet connection, Google’s speech-to-text function can make it very easy to add detailed descriptions to your story.

No matter how text is used, it needs to be succinct, clear, and informative.

## Text in the lead role

With breaking news stories, visuals and audio may not be immediately available. Some issue and event stories do not lend themselves to photo, video, or audio. These can be policy stories, visually uninteresting press conferences, or stories that are challenging because of lighting, noise, or access, or they are reported after the fact. If text is going to play a lead role in your presentation, it should have the following news-dispatch-like characteristics:

* Sentences should be short: 25-35 words. Use simple, declarative sentences.
* Get to the point quickly. The basic facts of the story should be in the first sentence or two. Draw your reader into the story with important information up top. Do not save the best for last.
* Think factoids (see the reporting lesson). Provide essential, basic information: who, what, where, when, and why.
* One post, one idea: Limit each post to a single point. There is nothing to prevent you from multiple posts, each about a different element of the larger story.
* Avoid inflammatory or biased language.
* Include relevant links and hashtags when possible.
* Think follow up: How can you use your text posts to build an audience for additional coverage of the topic throughout the story cycle?

## Examples of text in the lead role

* Dhaka, Bangladesh; November 25, 2012 — More than 100 people died Saturday and Sunday in a fire at a garment factory outside the capital.

* It took firefighters more than 17 hours to put out the blaze at the factory, Tazreen Fashions, after it started Saturday evening, retired fire official Salim Nawaj Bhuiyan said by telephone from Dhaka, Bangladesh’s capital.

* At least 111 people were killed, and many more workers were taken to several local hospitals and clinics for burns and smoke inhalation.

* “The main difficulty was to put out the fire. Sufficient approach road was not there,” said Salim Nawaj Bhuiyan, who now runs a fire safety company in Dhaka. “The fire service had to take great trouble to approach the factory.”

* Bangladesh’s garment industry is the second largest exporter of clothing after China. The industry employs more than 3 million workers, mostly women.

* The garment industry has a notoriously poor record of fire safety. Since 2006, more than 500 Bangladeshi workers have died in garment factory fires, according to Clean Clothes Campaign, an anti-sweatshop advocacy group based in Amsterdam.

* Experts say many of the fires could have been easily avoided if the factories had taken the right precautions. Many factories are in cramped neighborhoods, have too few fire escapes and widely flout safety measures, said the experts.

## Writing keywords and tags

Audiences outside of your social network and search engines interested in the topics you cover need to be able to find your stories. There is no way to search directly for a picture, video, or audio file. People can only search for text, so the keywords and tags associated with your media content is important. Keywords and tags should identify your story and relate it to broader topical themes of interest to your audience.

Keywords and tags can be written into the keyword or tags fields either in StoryMaker or on the platform on which the story will be published. In addition to the fields, keywords and tags can be included in a photo caption or a text post.

Guidelines for keyword and tag writing

* Use between 5 and 10 keywords or tags.
* Spell words out completely.
* Order the keywords or tags in terms of importance or likely search choice.
* Select clear, common descriptors. For example, use  “environmental” rather than “green.”
* Include broad descriptors for specific names used in the caption. For example, if “the Indira Gandhi Institute of Child Medicine” appears in the caption, use “children” and “hospital” as keywords or tags.
* Include alternative descriptors to those that appear in the caption.
* Include geographic location.
* Include important proper names.

---

## THINGS TO REMEMBER

* Keep it short.
* Stick to the facts.
* The standards of good journalism apply to you text posts.
* Use keywords and tags to help people find your stories.

---

## QUIZ

1. **Which best describes a good first sentence or text in the lead role?**

* 25-35 words long
* Enticing in the way that will draw the reader in
* A succinct, dramatic yet fact-based summary of the story
* All of the above


2. **True of false: Good writers do not have to be good reporters. After all, if you write well, its OK to make ideas appear as reported facts.**

* True
* False

3. **Which best describes characteristics of news writing that distinguish it from scholarly writing or literature?**

* News writing and microblogging feature thoughtful introductions that provide context before getting to the most current development of the story or idea.
* It should have long sentence written in an almost poetic style.
* News writing should include lots of obscure references, abbreviations, and jargon that assume the reader is informed about the background of the story.
* None of the above

---
