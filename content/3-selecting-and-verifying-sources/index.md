# Selecting and Verifying Sources

![](images/1.png)

#### How do you develop and think about identifying good characters to work as sources for your stories?

*Time to complete: 5-10 minutes*

---

![](images/2.png)

In this lesson you will learn:

* What a news source is
* How to identify good sources for your story
* How different sources play different roles in your story
* Rules for using anonymous and background sources

---

A good story is only as good as your characters. Journalists call these characters sources. Information from your sources is the driving force in most stories. Sources who participate as characters in your story are “on-the-record” sources. Some sources may provide background-only information that you use to investigate your story and confirm through on-the-record sources.

In addition to sources you identify at the scene of a news story, good journalists develop sources and contacts they can reach out to repeatedly.

## Types of sources

The most common source is a person you interview who is a character in a specific story. Sources come in five general categories:

* Reactionary sources
* Informational sources
* Authoritative or expert voice source
* Documents
* Social networking sites

Good news stories have at least two of the five, sometimes all five.

---

![](images/3.png)

## Reactionary sources

People on the scene and immediately involved or affected by the issue, such as:

* Victims of a bombing.
* People who are buying less (or not) as a result of the poor economy.

---

![](images/4.png)

## Informational sources

People, often officials, who can give you hard facts about the specific event, such as:

* An army officer or a hospital spokesperson in the event of a fire.
* An NGO spokesperson talking about the election or new financial regulation.

---

![](images/5.png)

## Authoritative or expert voice source

People who can give you a big-picture overview of the issue that your story is about, such as:

* Scholars, researchers, consultants.
* Someone from Doctors without Borders or an international medical expert on a story about lack of medical supplies at a local hospital, especially if this is part of a national trend.

---

![](images/6.png)

## Documents

Documents are often excellent sources. Physical documents can be found at government offices, libraries, and NGO reports. A huge trove of documents is now available online. Information from documentation can provide an authoritative or expert voice by giving the position of the organization on an important issue. Documents can also work as a background source.

---

![](images/7.png)

## Social networking sites

People and posts on social sites like Facebook or YouTube are sources. Groups exist around every conceivable topic. This is a good place to do background research and conduct interviews. It is important to identify yourself as a journalist when using social sites as reporting tools. It is also important to be transparent about your sources with editors and news organizations.

---

## Tips for finding and using sources

Always ask yourself if you have the best possible sources for your story — are they the most qualified? You should also ask yourself if you have a good variety of sources — are all sides of the issue represented? The more controversial the issue, the more difficult it is to find good sources.

#### Sources should never be made up

#### Transparency is key

Your sources should be clearly and completely identified.

#### Respect background-only agreements with sources

Sources may choose to give you information only on background. That means you cannot identify or quote them in your reporting. Background sources can be an excellent way to learn about your topic. For example, a background source in an important government ministry can tip you to stories before they happen and give you important information to use in your reporting. It is important to always respect the “background-only” agreement with a source. When you break these agreements with sources, they are not likely to keep providing you with valuable information.

---

## Attribution

Whenever you use someone else’s words or ideas, whether printed or spoken, attribute them to the source. Attribute information taken from news organizations, and provide a link if doing so. This will give the readers additional, valuable information. Do not use exact copy from a press release without attribution. Strive to go beyond the press release by relying on other sources. Do not use material from obscure websites or encyclopedia sites such as Wikipedia without confirming the information from the original source. Your viewers, readers, or listeners should be able to immediately identify the source of any information provided. Information taken directly from or paraphrased from other news sources is no exception.

---

### Identifying when to grant anonymity to a source

Some sources will ask for anonymity when they do not need it. Be careful about concealing the identity of such sources; it undermines your credibility as a reporter and negatively affects  the believability of your story. Granting anonymity to a source should only be used as a last resort.

Situations in which you might grant anonymity to a source include when your source’s life or job would be in danger if he/she were identified. In these rare cases, work with your source to determine what information you can use in your story to identify his or her place in the story. For example, “an official at the state department who spoke on the condition of anonymity because she is not authorized to speak to the press,” or “a member of the community whose life has been threatened and who is currently in hiding.”

---

## THINGS TO REMEMBER

* Your story will only be as good as your sources and characters.
* It is important to have a variety of sources for your story.
* There are different types of sources. Use them appropriately.
* Not all sources are characters in your story. Some of the best sources provide only background information.
* Be transparent about your sources and attribute material taken from other publications.

---

## QUIZ

1. **If you want to know how many people were injured in an earthquake, you might ask which of the following types of sources?**

* A reactionary source
* An authoritative source
* An informational source
* None of the above


2. **True or false: A background source is someone who agrees to speak to you off the record, meaning his or her name will not appear in the story. To be truthful, it is OK to publish the name.**

* True
* False

3. **In addition to talking to people, research and source documents are important parts of good journalism. Which are good sources for this kind of reporting?**

* RSS feeds
* Studies produced by nonprofit organizations or government
* Social sites, especially those that produce statistics about readers and viewers
* All of the above

---
