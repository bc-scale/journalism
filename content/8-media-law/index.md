# Media Law

![](images/1.png)

#### Media laws are regularly revised to silence journalists. How should you navigate the ongoing change in media law?

*Time to complete: 3-5 minutes*

---

In this lesson you will learn:

* What a code of ethics looks like
* What self-censorship is
* Tips to protect yourself as a journalist

---

Journalists everywhere work under at least a small degree of legal restrictions about what they can produce. Most commonly these are issues of national security, information deemed to be obscene, and language that might incite violence or put people in danger.

In countries with well-developed media laws, criticism of government and the presentation of unpopular ideas is permitted. Some degree of public access to government records is also guaranteed.

But some governments are more restrictive. New laws limit the media in general and access to the Internet in particular. Advocacy organizations such as the Committee to Protect Journalists are concerned that vague language written into emerging laws will be used against journalists.

---

## What should you do to protect yourself from the overly restrictive application of media laws?

---

![](images/2.png)

### Know the law

Understand the law in the country or region where you work. Just because laws to protect journalists exist does not mean the government, local leadership, and security forces will obey the laws. However, understanding the law can be a first step toward improving it.

---

![](images/3.png)

### Adhere to professional codes of conduct

In the lesson on ethics there is a professional code of conduct for journalists. Even in countries where media laws are very restrictive, observing the principles outlined can help you avoid accusations of libel or treason.

---

![](images/4.png)

### Advocate for change

One of the key characteristics of the shift in media across the globe is the blending of media with media advocacy. Citizen journalists are breaking through barriers in how journalism is defined at the same time as they are covering stories. Pool resources with others to create a knowledge base. Seek support from legal organizations within your country. Seek support and information from international organizations such as the Committee to Project Journalists.

---

### Be aware of self-censorship

Even in many countries with fairly liberal laws, self-censorship is an issue among journalists. Some journalists are uncomfortable covering highly controversial cultural issues, such as the rights of women or tribal disputes. Some journalists are reluctant to cover issues that tarnish national image such as widespread alcohol abuse or corruption within popular institutions. But if these issues have relevance to the public interest, people need to know, and it is your job to tell them.

---

![](images/5.png)

### Protect yourself (and your sources)

This issue is addressed more fully in the digital security lessons. In short, understanding the law, how authorities may ignore or abuse the law or identify you online, and being proactive are all essential for any journalist working under unsafe conditions. If you are arrested, prosecuted, or harassed, contact national and international organizations that provide support for journalists.

---

## THINGS TO REMEMBER
* Media law will develop differently in different regions and countries.
* Build a community of journalists and work to advocate for the community.
* Be aware of self-censorship among journalists.
* Adhere to a professional code of conduct.
* Invest in your own digital security.

---

## QUIZ

1. **Even if the media laws are unclear and the authorities apply them unfairly in the country or region where you work, which of the following can help citizen journalists protect themselves?**

* Be familiar with the laws that are on the books.
* Advocate for better media law.
* Adhere to a professional code of ethics.
* All of the above

2. **True or false: Self-censorship can be an issue for many journalists even if government censorship is not. Good journalists need to be willing to give fair coverage to ideas with which they do not agree, unpopular topics that are relevant to public interest, and subject matter that is culturally uncomfortable but important.**

* True
* False

---
