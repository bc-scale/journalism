# Social Media Basics

![](images/1.png)

#### How can I best leverage social media tools and networks to reach the broadest possible audience with information about important issues?

*Time to complete: 5-10 minutes*

---

![](images/2.png)

In this lesson you will learn:

*	What editors look for in stories from social media
*	How to use social media as a reporting tool
*	How to decide what medium you should use for your story
*	What steps you can take to help your story get discovered

---

Social platforms can reach an unprecedented international audience. In the Middle East and elsewhere during the last few years, several significant stories appeared on Twitter, Facebook and YouTube before they reached television outlets such as the BBC, CNN, or Al Jazeera. The combination of a viral story online and television coverage can quickly make an issue a leading news story and force a response from the governments or other powerful institutions.

![](images/3.png)

Top news organizations cull social media for coverage. They are looking for compelling stories that are well-presented and verifiable. Citizen journalists who understand what large news organizations are looking for — and what their concerns are — can build longstanding relationships and an unparalleled chance to get their stories viewed by those who influence policy both nationally and internationally.

For most large news organizations, credibility and high standards for the presentation of information are key concerns in deciding to use material produced by citizen journalists. Below are some basic tips for addressing this. We will discuss these and many others in more detail in the following sections in this section and in the other sections:

---

## Tips

* Begin to build an online track record for yourself as a journalist through a profile on social networking sites. NOTE: Safety can be an issue. Remember: Pseudonymity is not the same thing as anonymity. (See the security section for more on this.)

* Be informed on the issues you cover. Become an expert. This will help you provide better content and stay ahead of the pack.

* Be as neutral as possible in the tone of your coverage, no matter how dramatic the event or how passionately you feel about it.

* In discussions with editors, be transparent about your coverage techniques and any potential conflicts of interest you may have.

* Attribute information in your report to specific sources, and provide contact information for sources when possible so reporting can be verified. (More on this in the reporting section of this section.)

* Smart text in the form of captions, keywords, tags, titles, and stories is essential even for video, photo, and audio stories. Search engines and the editors who use them will not be able to find your story without good search terms associated with your story. Furthermore, once editors find your story, the text will help them understand and verify your reporting. (More on this in the next section).

* No matter the tool, the presentation should be technically professional. Shaky and out-of-focus video with lots of zooms and pans is an example of content that is not technically professional. (More on this in the video section.)

* Try to understand what sort of story you are doing and how it fits into broader coverage of the issue. Different kinds of stories are structured in unique ways. For example, a breaking story about a bombing will take a different form — require a different style of presentation and even different information — than a profile about an injured rebel fighter and his feelings about an upcoming election. In addition, large news organizations often combine several stories of different types to create a “package.” Understand how your story will fit into this broader coverage. (More on this in the section on story types.)

* Do not present what you do not know or cannot verify. A more limited but completely accurate story is more valuable than one that is far-reaching but based on conjecture.

* It is important to become part of groups, boards and other discussion related to your issues or regions of coverage.

### Example of a seamless content on Twitter

![](images/4.jpg)

In a social-first news environment, your coverage may begin with little more than a short SMS or photo of a breaking news event. As you learn more, your coverage will become more in-depth, and the medium you use may change; you need to always choose the best tool to tell your story. For some stories and some audiences, video will be better. For others, audio.

No matter the medium, tool, and audience, social coverage of any story is always evolving. This is called the news cycle. It is important to be aware of how other journalists and news organizations are using social media to cover the issue through the cycle. If you are working as part a group project or with a larger news organization, it is important to understand how your coverage fits into the overall news strategy.

---

### THINGS TO REMEMBER

* Social tools and platforms work very well for breaking news. Take advantage of that.

* Your credibility as a citizen journalist in the social-first environment is essential to news organizations.

* As the story evolves, so should your coverage.

* Understand how your coverage of an issue best contributes to coverage of the organization you are working with.

---

### QUIZ

1. **Which of the following is important for a citizen journalist to do? (There may be more than one answer.)**

* Build an online presence for yourself through social sites.
* No matter your tools, your work should always look technically professional.
* Your projects should be based on things that are interesting only to you, and your presentation should be based on your own opinion and second-hand information,
* All of the above

2. **True or false: Two key elements that define the best citizen journalism are that it often takes the form of a dialogue with the audience, and the journalist is transparent about reporting methods, sources, and influences.**

* True
* False

3. **What are some of the biggest challenges for citizen journalists in their coverage?**

* Little or no research prior to an event
* Access to key figures or locations in the story
* An inability to reach an audience through large national and international outlets
* All of the above

---
